# Updog logging template

A basic example of a logging extension for [Updog](https://git.askiiart.net/askiiart/updog). And this is the only example/template extension that actually does anything useful!

## Usage

Arguments:

- `file`: The absolute path to the log file.
