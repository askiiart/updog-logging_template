class LoggingTemplate:
    def __init__(self, arguments):
        self.log_filename = arguments['file']

    def log(self, service_name, code, status):
        # there's definitely a better way to do this but i'm not gonna bother
        log_file = open(self.log_filename, 'at')
        log_file.write(f'{service_name} {code}: {status}\n')
        log_file.close()
